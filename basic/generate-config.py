#!/bin/python python3

import csv

variables = "configuration-values.csv"
template = "configuration-template.config"


class Lab5:

    def __init__(self, variables, template):
        self.vars = {}
        self.template = ""
        self.read_vars(variables)
        self.read_template(template)

        # https://stackoverflow.com/questions/3097866/access-an-arbitrary-element-in-a-dictionary-in-python
        for i in range(len(list(self.vars.values())[0])):
            self.new_config(i, i + 1)

    def read_vars(self, variables):
        values = []
        with open(variables, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            for row in reader:
                values.append(row)
        self.seperate(values)

    def seperate(self, values):
        # https://stackoverflow.com/questions/2468334/python-how-to-create-dynamic-and-expandable-dictionaries#2468374
        for row in range(len(values)):
            self.vars[values[row][0]] = values[row][1:]

    def read_template(self, template):
        with open(template, "r") as config_template:
            self.template = config_template.read()

    def new_config(self, key, hostname):
        new = self.template
        for var in self.vars.keys():
            new = new.replace(var, self.vars[var][key])

        with open(hostname + ".config", "a") as config_file:
            config_file.write(new)


Lab5(variables, template)
