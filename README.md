# Cisco IOS Configuration Generator
Generate network configuration from a template and CSV-variables.
See sample-output for generated example configuration.

## 3 Versions
 - basic
	a basic proof-of-concept implementation
 - error-handeling
	builds on the basic implementation and adds error-handeling
 - more-PEP8
	addapts the error-handeling version to better comform to PEP8


## Usage
generate-config.py [-h] _variables_ _template_

positional arguments:
 - _variables_   csv file containting your variables
 - _template_    config-file with variable placeholders

optional arguments:
 - -h, --help  show this help message and exit
