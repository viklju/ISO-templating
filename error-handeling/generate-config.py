#!/usr/bin/env python3

import csv
import warnings
import os.path  # replace with pathlib
import re
import argparse
import sys

# ------------------------------------------------------------------------------
# Optionally specify using commandline arguments
variables = ""  # "configuration-values.csv"
template = ""  # "configuration-template.config"

# ------------------------------------------------------------------------------

# ADD SOME BASIC DOCUMENTATION (AND EXPAND ARGPARSE)
# CHMOD +x


class Lab5:

    def __init__(self, variables, template):
        self.variables_file = variables
        self.template_file = template

        self.vars = {}
        self.config = ""

        self.check_file(self.variables_file)
        self.check_file(self.template_file)
        self.read_vars()
        self.read_template()
        # https://stackoverflow.com/questions/3097866/access-an-arbitrary-element-in-a-dictionary-in-python
        for i in range(len(list(self.vars.values())[0])):
            self.new_config(i, i + 1)

    def check_file(Self, file):
        if not os.path.isfile(file):
            raise IOError('Error: Could not find file\n  file: "' + file + '"')
        if not os.path.getsize(file) > 0:
            raise NotImplementedError('"' + file + '" is empty')

    def read_vars(self):
        values = []
        with open(self.variables_file, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', quotechar='"')

            line = 0
            for row in reader:
                line += 1
                if row[0].upper().startswith("VAR_"):
                    values.append(row)
                else:
                    raise SyntaxError('"' + self.variables_file + '"\n Variable does not begin with "var_" (variable: "' + row[0] + '", line: ' + str(line) + ')')
        self.seperate(values)

    def seperate(self, values):
        compare = len(values[0])
        line = 0
        for row in range(len(values)):
            line += 1
            if values[row][0] not in self.vars.keys():
                self.vars[values[row][0]] = values[row][1:]
            else:
                raise SyntaxError('"' + self.variables_file + '"\n Duplicate variable at line ' + str(line))

            if compare != len(values[row]):
                raise SyntaxError('"' + self.variables_file + '"\n Too many variables, lenght of line 1 and line ' + str(line) + ' differs')

    def read_template(self):
        with open(self.template_file, 'r') as config_template:
            self.config = config_template.read()
        if 'VAR_' not in self.config.upper():
            raise SyntaxError('"' + self.template_file + '"\n Could not find any variables to replace in template')

    def new_config(self, key, filename):
        if os.path.isfile(str(filename) + '.config'):
            raise IOError('Error: Could not create new config-file\n  Config-file: "' + str(filename) + '.config' + '" already exist')

        new = self.config
        for var in self.vars.keys():
            if var in new:
                # https://stackoverflow.com/questions/919056/case-insensitive-replace
                regex = re.compile(re.escape(var), re.IGNORECASE)
                new = regex.sub(self.vars[var][key], new)
            else:
                warnings.warn(var + ' not found in template...', stacklevel=4)

        placeholders = re.findall(r'var\_.*', new, 2)  # flag=2 --> IGNORECASE
        if placeholders != []:
            raise SyntaxError('"' + self.template_file + '"\n Template file contains variable(s) not found in "' + self.variables_file + '"\n  Missing variables: ' + str(placeholders))

        with open(str(filename) + '.config', 'a') as config_file:
            config_file.write(new)


# https://stackoverflow.com/questions/4042452/display-help-message-with-python-argparse-when-script-is-called-without-any-argu
class MyParser(argparse.ArgumentParser):
    def error(self, message):
        # sys.stderr.write('Error: %s\n' % message)
        self.print_help()
        sys.exit(3)


try:
    if variables == "" or template == "":
        parser = MyParser(description="Create config files from a template and csv file")
        parser.add_argument("variables", help="csv file containting your variables")
        parser.add_argument("template", help="config-file with variable placeholders")
        args = parser.parse_args()
        variables = args.variables
        template = args.template

    Lab5(variables, template)
except SyntaxError as e:
    print('SyntaxError:', e)
except IOError as e:
    print('IOError: when accessing file\n', e)
except NotImplementedError as e:
    print('NotImplementedError: when checking file\n', e)
