#!/usr/bin/env python3
"""module docstring"""

from __future__ import print_function
import csv
import warnings
import os.path
import re
import argparse
import sys

# REPLACE os.path WITH pathlib
# EXPAND ARGPARSE?
# BETTER VARIABLE NAMES?


class Lab5:  # C1001 old-styled class defined
    """class docstring"""

    def __init__(self, variables, template):
        self.variables_file = variables
        self.template_file = template

        self.vars = {}
        self.config = ""

        self.check_file(self.variables_file)
        self.check_file(self.template_file)
        self.read_vars()
        self.read_template()

        key = 0
        while key < len(list(self.vars.values())[0]):
            self.new_config(key, key + 1)
            key += 1

    def check_file(self, file):  # W0622 redefined-builtin 'file'
        """method docstring"""

        if not os.path.isfile(file):
            raise IOError('Error: Could not find file\n  file: "' + file + '"')
        if not os.path.getsize(file) > 0:
            raise NotImplementedError('"' + file + '" is empty')

    def read_vars(self):
        """method docstring"""

        values = []
        with open(self.variables_file, 'r') as csv_file:
            reader = csv.reader(csv_file, delimiter=',', quotechar='"')
            line = 0
            for row in reader:
                line += 1
                if row[0].upper().startswith("VAR_"):
                    values.append(row)
                else:
                    raise SyntaxError('"' + self.variables_file
                                      + '"\n Variable does not begin with'
                                      + '"var_" (variable: "' + row[0]
                                      + '", line: ' + str(line) + ')')
        self.seperate(values)

    def seperate(self, values):
        """method docstring"""

        compare = len(values[0])
        line = 0
        for row in values:
            line += 1
            if row[0] not in self.vars.keys():
                self.vars[row[0]] = row[1:]
            else:
                raise SyntaxError('"' + self.variables_file
                                  + '"\n Duplicate variable at line '
                                  + str(line))
            if compare != len(row):
                raise SyntaxError('"' + self.variables_file + '"\n Too many '
                                  + 'variables, lenght of line 1 and line '
                                  + str(line) + ' differs')

    def read_template(self):
        """method docstring"""

        with open(self.template_file, 'r') as config_template:
            self.config = config_template.read()
        if 'VAR_' not in self.config.upper():
            raise SyntaxError('"' + self.template_file + '"\n Could not '
                              + 'find any variables to replace in template')

    def new_config(self, key, filename):
        """method docstring"""

        if os.path.isfile(str(filename) + '.config'):
            raise IOError('Error: Could not create new config-file\n  '
                          + 'Config-file: "' + str(filename) + '.config'
                          + '" already exist')

        new = self.config
        for _, var in enumerate(self.vars):
            if var in new:
                regex = re.compile(re.escape(var), re.IGNORECASE)
                new = regex.sub(self.vars[var][key], new)
            else:
                warnings.warn(var + ' not found in template...', stacklevel=4)

        placeholders = re.findall(r'var\_.*', new, 2)  # flag=2 --> IGNORECASE
        if placeholders != []:
            raise SyntaxError('"' + self.template_file + '"\n Template file '
                              + 'contains variable(s) not found in "'
                              + self.variables_file
                              + '"\n  Missing variables: ' + str(placeholders))

        with open(str(filename) + '.config', 'a') as config_file:
            config_file.write(new)


# https://stackoverflow.com/questions/4042452/display-help-message-with-python-argparse-when-script-is-called-without-any-argu
class MyParser(argparse.ArgumentParser):
    """class docstring"""

    def error(self, message):
        self.print_help()
        sys.exit(3)


try:
    PARSER = MyParser(description="Create config files from a template "
                      + "and csv file")
    PARSER.add_argument("variables", help="csv file containting your variables")
    PARSER.add_argument("template", help="config-file with variable "
                        + "placeholders")
    ARGS = PARSER.parse_args()
    VARIABLES = ARGS.variables
    TEMPLATE = ARGS.template

    Lab5(VARIABLES, TEMPLATE)
except SyntaxError as error:
    print('SyntaxError:', error)
except IOError as error:
    print('IOError: when accessing file\n', error)
except NotImplementedError as error:
    print('NotImplementedError: when checking file\n', error)
